<?php

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 'on');

require '../vendor/autoload.php';

use ContactForm\Configuration\Form;
use ContactForm\Configuration\Mail;
use ContactForm\Configuration\SMTP;
use ContactForm\ContactForm;
use ContactForm\Validate\StopForumSpam;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

$logger = new Logger('Contact Form Test');
$logger
    ->pushHandler(new StreamHandler('php://stdout'))
    ->pushHandler(new RotatingFileHandler(__DIR__ . '/debug.log', 0, Logger::NOTICE))
;

/**
 * Define SMTP settings
 */
$smtp = new SMTP('709fc8b0135c1d', '9dbd8257be31c9', 'smtp.mailtrap.io', 2525);

/**
 * Define form settings
 */
$form = new Form('contact', 'name', 'email');
$form->defineInputFieldSensitive('password');

/**
 * Init class and set configuration
 */
$contactForm = new ContactForm($form, $smtp);
$contactForm->setLogger($logger);
$contactForm->addSpamValidation(new StopForumSpam());

/**
 * Status
 */
$mailSentInCurrentSubmit = false;
$mailSentInPreviousSubmit = false;

if ($contactForm->isSubmitValid($_POST)) {
    $mail = new Mail(
        'New Contact',
        'contact-form@bitandblack.com',
        $form->getData('message')
    );
    $mail
        ->setReplyToName($form->getName())
        ->setReplyToMail($form->getEmail())
    ;
    
    $contactForm->addMail($mail);
    
    $mailSentInCurrentSubmit = $contactForm->sendMail();
}

$mailSentInPreviousSubmit = $contactForm->hasSentMailPreviously();

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>ContactForm Example</title>
    </head>
    <body>
        <?php
        
        /**
         * Display message when a mail has been sent
         */
        if ($mailSentInCurrentSubmit || $mailSentInPreviousSubmit) {
            echo '
                <p>
                    Your mail has been sent.
                </p>
            ';
        }
        
        ?><form action="" method="post">
            <input type="text" name="contact[name]" title="Your Name" placeholder="John Doe">
            <input type="email" name="contact[email]" title="Your Email Address" placeholder="john.doe@online.de">
            <textarea name="contact[message]" title="Your Message" placeholder="Dear Sir or Madame, ..."></textarea>
            <button type="submit">Send</button><?php
            
            /**
             * Display additional fields that are used to verify the submit.
             */
            echo $contactForm->getAdditionalFields();
              
            ?></form>
    </body>
</html>

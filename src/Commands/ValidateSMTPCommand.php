<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Terminal;

class ValidateSMTPCommand extends Command
{
    private bool $tableHasRows = false;

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('validate:smtp')
            ->setDescription('Tests the SMTP connection.')
            ->setHelp('Tests the SMTP connection.')
            ->addArgument('host', InputArgument::REQUIRED)
            ->addArgument('port', InputArgument::REQUIRED)
            ->addArgument('user', InputArgument::REQUIRED)
            ->addArgument('password', InputArgument::REQUIRED)
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $host = (string) $input->getArgument('host');
        $port = (int) $input->getArgument('port');
        $user = (string) $input->getArgument('user');
        $password = (string) $input->getArgument('password');

        $success = false;
        $message = '<error>Error</error>';

        $fsock = fsockopen($host, $port);
        
        if (false === $fsock) {
            $output->writeln('Failed to open connection.');
            return -1;
        }
        
        $res = fread($fsock, 1024);

        if (false === $res) {
            $output->writeln('Failed to read response.');
            return -1;
        }
        
        if ('' !== $res && 0 === strpos($res, '220')) {
            $success = true;
            $message = '<info>Success</info>';
        }

        $output->writeln('Connection result: ' . $message . '. ');
        
        if (!$success) {
            return -1;
        }
        
        $output->writeln('The details are: ');

        $terminal = new Terminal();
        $width = $terminal->getWidth();
        
        if ($width > 120) {
            $width = 120;
        }
        
        $table = new Table($output);
        $table->setHeaders(['Request', 'Response']);
        $table->setStyle('box');
        $table->setColumnMaxWidth(0, $width / 2);
        $table->setColumnMaxWidth(1, $width / 2);

        $from = 'from@test.de';
        $to = 'to@test.de';
        $nameFrom = 'From Me';
        $nameTo = 'To Me';
        $subject = 'Test subject.';
        $mailMessage = 'Test message.';
            
        $localhost = gethostbyname(
            (string) gethostname()
        );
        $newLine = "\r\n";
        
        //Request Auth Login
        $this->executeRequest($fsock, 'AUTH LOGIN' . $newLine, $table);
        
        //Send username
        $this->executeRequest($fsock, base64_encode($user) . $newLine, $table, 'User');

        //Send password
        $this->executeRequest($fsock, base64_encode($password) . $newLine, $table, 'Password');
    
        //Say Hello to SMTP
        $this->executeRequest($fsock, 'HELO ' . $localhost . $newLine, $table);
        
        //Email From
        $this->executeRequest($fsock, 'MAIL FROM: ' . $from . $newLine, $table);
        
        //Email To
        $this->executeRequest($fsock, 'RCPT TO: ' . $from . $newLine, $table);
        
        //The Email
        $this->executeRequest($fsock, 'DATA' . $newLine, $table);
        
        //Construct Headers
        $headers = 'MIME-Version: 1.0' . $newLine;
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . $newLine;
        $headers .= 'To: ' . $nameTo . ' <' . $to . '>' . $newLine;
        $headers .= 'From: ' . $nameFrom . ' <' . $from . '>' . $newLine;
        
        $string = 'To: ' . $to . PHP_EOL . 'From: ' . $from . PHP_EOL . 'Subject: ' . $subject . PHP_EOL .
            $headers . PHP_EOL . PHP_EOL . $mailMessage . PHP_EOL . '.'
        ;
        
        $this->executeRequest($fsock, $string . $newLine, $table, trim(str_replace(["\r\n", "\n\n", "\n"], [', ', '', ', '], $string), '., '));
        
        // Say Bye to SMTP
        $this->executeRequest($fsock, 'QUIT' . $newLine, $table);
        
        $table->render();
        fclose($fsock);
        
        return 0;
    }

    /**
     * @param resource $fsock
     * @param string $string
     * @param Table $table
     * @param string|null $description
     * @return ValidateSMTPCommand
     */
    private function executeRequest(
        $fsock,
        string $string,
        Table $table,
        ?string $description = null
    ): self {
        fwrite($fsock, $string);
        $smtpResponse = fgets($fsock, 515);
        
        if ($this->tableHasRows) {
            $table->addRow(new TableSeparator());
        }
        
        $table->addRow([
            trim($description ?? $string),
            trim((string) $smtpResponse),
        ]);
        
        if (!$this->tableHasRows) {
            $this->tableHasRows = true;
        }
        
        return $this;
    }
}

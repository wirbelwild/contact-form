<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm;

use ContactForm\Configuration\Form;
use ContactForm\Configuration\Mail;
use ContactForm\Configuration\SMTP;
use ContactForm\Mailer\MailerInterface;
use ContactForm\Mailer\PHPMailerBridge;
use ContactForm\Secure\Crypt;
use ContactForm\Secure\UniqueToken;
use ContactForm\Validate\ValidationInterface;
use Exception;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Handles the form and its submit
 */
class ContactForm implements LoggerAwareInterface
{
    /**
     * If an SMTP connection is enabled
     */
    private bool $isSMTPEnabled = false;

    /**
     * A status message
     */
    private string $status = '';

    private LoggerInterface $logger;

    private MailerInterface $mailer;

    /**
     * @var ValidationInterface[]
     */
    private array $validation = [];

    /**
     * How long it should take at least to fill out the form in seconds
     */
    private int $timeMinimum = 5;

    /**
     * How long the submit session should go in seconds
     */
    private int $submitSessionLength = 120;

    private Form $form;

    /**
     * @var Mail[]
     */
    private array $mails = [];

    private ?SMTP $smtp;

    private UniqueToken $uniqueToken;

    private Crypt $crypt;

    /**
     * If the REQUEST_METHOD should be proofed
     */
    private bool $handleRequestStrict = false;

    /**
     * Init
     *
     * @param Form $form
     * @param SMTP|null $smtp
     * @throws Exception
     */
    public function __construct(Form $form, ?SMTP $smtp = null)
    {
        if (session_status() === PHP_SESSION_NONE) {
            @session_start();
        }

        $this->form = $form;
        $this->smtp = $smtp;
        $this->logger = new NullLogger();
        $this->uniqueToken = new UniqueToken();
        $this->crypt = new Crypt();

        $this->setCrypt();
    }

    /**
     * Sets a logger instance
     *
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
        $this->logger->debug('Init Contact Form');

        if (null !== $this->smtp) {
            $this->smtp->setLogger($logger);
        }
    }

    /**
     * @param MailerInterface $mailer
     * @return ContactForm
     */
    public function setMailer(MailerInterface $mailer): self
    {
        $this->mailer = $mailer;
        return $this;
    }

    /**
     * @param ValidationInterface $validation
     * @return ContactForm
     */
    public function addSpamValidation(ValidationInterface $validation): self
    {
        if (!in_array($validation, $this->validation)) {
            $this->validation[] = $validation;
        }

        return $this;
    }

    /**
     * @param Mail ...$mail
     * @return ContactForm
     * @deprecated This method is deprecated, please use `addMail()` instead.
     * @see \ContactForm\ContactForm::addMail()
     */
    public function setMail(Mail ...$mail): self
    {
        array_push($this->mails, ...$mail);
        $this->mails = array_unique($this->mails, SORT_REGULAR);
        return $this;
    }

    /**
     * Adds one or multiple emails.
     *
     * @param Mail ...$mail
     * @return ContactForm
     */
    public function addMail(Mail ...$mail): self
    {
        array_push($this->mails, ...$mail);
        $this->mails = array_unique($this->mails, SORT_REGULAR);
        return $this;
    }

    /**
     * Returns the current status
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * Handle salts coming from and going into session.
     *
     * @return bool
     * @throws Exception
     */
    private function setCrypt(): bool
    {
        if (!isset($_SESSION['contactForm']['vector'], $_SESSION['contactForm']['key'])) {
            $this->logger->debug('New salt created');
            $this->crypt->createSalt();

            $_SESSION['contactForm']['vector'] = base64_encode($this->crypt->getVector());
            $_SESSION['contactForm']['key'] = base64_encode($this->crypt->getKey());

            return true;
        }

        $this->crypt
            ->setVector((string) base64_decode($_SESSION['contactForm']['vector']))
            ->setKey((string) base64_decode($_SESSION['contactForm']['key']))
        ;

        return true;
    }

    /**
     * Checks the POST data if a submitted form is valid
     *
     * @param array<string, mixed> $post
     * @return bool
     */
    public function isSubmitValid(array $post): bool
    {
        if ($this->handleRequestStrict && !$this->proofRequest()) {
            return false;
        }

        $namespace = $this->form->getNamespace();

        if (!array_key_exists($namespace, $post)) {
            return false;
        }

        $this->restoreHashes();

        foreach ($this->decryptFormData($post, $namespace) as $key => $value) {
            $this->form->setData(
                mb_strtolower((string) $key),
                $value
            );

            $valueToLog = in_array($key, $this->form->getSensitiveInputFields(), false)
                ? '*** SENSITIVE INFORMATION ***'
                : $value
            ;

            $this->logger->debug('Decrypted', [$key, $valueToLog]);
        }

        $uniqueTokenExists = '' !== $this->form->getUniqueToken();
        $uniqueTokenIsValid = $this->uniqueToken->isTokenValid($this->form->getUniqueToken());

        if (!$uniqueTokenExists || !$uniqueTokenIsValid) {
            $this->status = 'ABORTION_TOKEN_INVALID';
            $this->logger->notice('Token is invalid');
            $this->logger->debug('UniqueToken exists:', [$uniqueTokenExists]);
            $this->logger->debug('UniqueToken is valid:', [$uniqueTokenIsValid]);
            return false;
        }

        $timeStampExists = 0 !== $this->form->getTimeStamp();
        $neededTime = time() - $this->form->getTimeStamp();
        $timeStampIsValid = $neededTime >= $this->timeMinimum;

        if (!$timeStampExists || !$timeStampIsValid) {
            $this->status = 'ABORTION_TIMESTAMP_INVALID';
            $this->logger->notice('TimeStamp is invalid');
            $this->logger->debug('TimeStamp exists:', [$timeStampExists]);
            $this->logger->debug('TimeStamp is valid:', [$timeStampIsValid]);
            $this->logger->debug('Needed time is:', [$neededTime]);
            return false;
        }

        if ($this->form->getData($this->form->getHoneyPotFieldName()) !== '') {
            $this->status = 'ABORTION_TRAPPED_IN_HONEYPOT';
            $this->logger->notice('Trapped in honeypot');
            return false;
        }

        if (!$this->getExternalValidation()) {
            $this->status = 'ABORTION_BY_EXTERNAL_VALIDATION';
            $this->logger->notice('Abortion by external validation');
            return false;
        }

        $this->status = 'SUBMIT_IS_VALID';
        $this->logger->notice('Submit is valid.');
        return true;
    }

    /**
     * Connects some external validations
     *
     * @return bool
     */
    private function getExternalValidation(): bool
    {
        if (empty($this->validation)) {
            $this->logger->debug('There is no external validation configured');
            return true;
        }

        foreach ($this->validation as $validation) {
            if ($validation->isEmailBad($this->form->getEmail())) {
                $this->logger->notice('Email recognized bad', [$this->form->getEmail()]);
                return false;
            }

            $remoteAddr = $_SERVER['REMOTE_ADDR'] ?? '';
            $remoteAddrBad = false;
            $forwardedFor = $_SERVER['HTTP_X_FORWARDED_FOR'] ?? '';
            $forwardedForBad = false;

            if (null !== $remoteAddr) {
                $remoteAddrBad = $validation->isIpBad($remoteAddr);
            }

            if (null !== $forwardedFor) {
                $forwardedForBad = $validation->isIpBad($forwardedFor);
            }

            if ($remoteAddrBad || $forwardedForBad) {
                $this->logger->notice('IP recognized bad');
                return false;
            }
        }

        return true;
    }

    /**
     * Restores vector and key from session
     *
     * @return ContactForm
     */
    private function restoreHashes(): self
    {
        $hashVector = base64_decode($_SESSION['contactForm']['vector']);
        $hashKey = base64_decode($_SESSION['contactForm']['key']);

        $this->crypt = new Crypt();
        $this->crypt->setVector((string) $hashVector);
        $this->crypt->setKey((string) $hashKey);

        return $this;
    }

    /**
     * Stores and decrypts form data
     *
     * @param array<string, mixed> $post
     * @param string $namespace
     * @return array<int|string, mixed>
     */
    private function decryptFormData(array $post, string $namespace): array
    {
        $formData = [];

        foreach ($post[$namespace] as $key => $value) {
            if (0 === strpos($key, 'cf_')) {
                $key = substr($key, 3);
                $key = $this->crypt->decryptData($key);
                $value = $this->crypt->decryptData($value);
                $formData[$key] = $value;
                continue;
            }

            $formData[$key] = $value;
        }

        return $formData;
    }

    /**
     * Sends email
     *
     * @return bool
     */
    public function sendMail(): bool
    {
        if (null !== $this->smtp) {
            if (null === parse_url($this->smtp->getHost(), PHP_URL_SCHEME)) {
                $this->logger->debug(
                    'The SMTP host is missing a scheme. This could result in problems.',
                    [
                        'host' => $this->smtp->getHost(),
                    ]
                );
            }
            $this->isSMTPEnabled = $this->smtp->isSMTPConnectionValid();
        }

        if (null === $this->mailer) {
            $this->setDefaultMailer();
        }

        $success = true;
        $mailCount = count($this->getMails());
        $this->logger->debug('A total of ' . $mailCount . ' have been defined.');

        foreach ($this->getMails() as $key => $mail) {
            $this->logger->debug('Trying to send mail ' . ($key + 1) . ' of ' . $mailCount . '.');

            try {
                $mailer = clone $this->mailer;

                if (null === $mailer->getFromAddress()) {
                    $mailer->setFromAddress($this->form->getEmail());
                }

                if (null === $mailer->getFromName()) {
                    $mailer->setFromName($this->form->getName());
                }

                if (null === $mailer->getSubject()) {
                    $mailer->setSubject($mail->getSubject());
                }

                if (null === $mailer->getMessageHTML()) {
                    $mailer->setMessageHTML($mail->getMessage());
                }

                if (null === $mailer->getToAddress()) {
                    $mailer->setToAddress($mail->getReceiver());
                }

                if ((null === $mailer->getSender())
                    && null !== $sender = $mail->getSender()
                ) {
                    $mailer->setSender($sender);
                }

                if (null !== $replyToMail = $mail->getReplyToMail()) {
                    $mailer->setReplyToAddress($replyToMail);
                    $mailer->setReplyToName($replyToMail);
                }

                if (null !== $replyToName = $mail->getReplyToName()) {
                    $mailer->setReplyToName($replyToName);
                }

                /** Adding attachments that were added to a mail. */
                if ([] !== $attachments = $mail->getAttachments()) {
                    foreach ($attachments as $attachment) {
                        $mailer->addAttachment($attachment['path'], $attachment['name']);
                    }
                }

                $mailer->setHTML($mail->isHtml());

                if ($this->isSMTPEnabled && null !== $this->smtp) {
                    $this->setSMTPConfiguration($mailer, $this->smtp);
                }

                if (!$mailer->send()) {
                    $this->logger->notice('Failed sending mail.', $mailer->getDebugInformation());
                    $success = false;
                }
            } catch (Exception $exception) {
                $this->logger->notice($exception->getMessage());
                $success = false;
            }
        }

        $_SESSION['contactForm']['sentMail'] = $success;

        if (true === $success) {
            $_SESSION['contactForm']['sendTime'] = time();
            $this->status = 'SENT_MAIL_SUCCESSFULLY';
            $this->logger->notice('Successfully sent mails.');
        }

        return $success;
    }

    /**
     * @return ContactForm
     */
    private function setDefaultMailer(): self
    {
        $this->mailer = new PHPMailerBridge();
        $this->mailer
            ->setCharSet('UTF-8')
            ->setContentType("text/html; charset=utf-8\r\n")
            ->setEncoding('8bit')
            ->setWordWrap(80)
        ;

        return $this;
    }

    /**
     * Returns special fields that should be appended to a form
     *
     * @return string
     * @throws Exception
     */
    public function getAdditionalFields(): string
    {
        $namespace = $this->form->getNamespace();

        $uniqueTokenKey = $this->crypt->encryptData('uniqueToken');
        $uniqueTokenValue = $this->crypt->encryptData($this->uniqueToken->getToken());

        $timeStampKey = $this->crypt->encryptData('timeStamp');
        $timeStampValue = $this->crypt->encryptData((string) time());

        return '
            <div style="display: none;" class="contact-form-additional-fields">
                <input type="hidden" name="' . $namespace . '[cf_' . $uniqueTokenKey . ']" value="' . $uniqueTokenValue . '">
                <input type="hidden" name="' . $namespace . '[cf_' . $timeStampKey . ']" value="' . $timeStampValue . '">
                <input type="text" name="' . $namespace . '[' . $this->form->getHoneyPotFieldName() . ']" value="" autocomplete="off" tabindex="-1">
            </div>
        ';
    }

    /**
     * Returns if a mail has been sent or not in the previous submit.
     * When the sentTime length has ended, the status will be reset to enable new submits
     *
     * @return bool
     */
    public function hasSentMailPreviously(): bool
    {
        if (isset($_SESSION['contactForm']['sentMail'])) {
            $sendTime = $_SESSION['contactForm']['sendTime'] ?? time();
            $sendStatus = $_SESSION['contactForm']['sentMail'];

            if ($sendTime + $this->submitSessionLength < time()) {
                unset($_SESSION['contactForm']['sentMail'], $_SESSION['contactForm']['sendTime']);
            }

            return $sendStatus;
        }

        return false;
    }

    /**
     * Configure SMTP
     *
     * @param MailerInterface $mailer
     * @param SMTP $connection
     * @return ContactForm
     */
    private function setSMTPConfiguration(MailerInterface $mailer, SMTP $connection): self
    {
        $mailer->setSMTP(true);
        $mailer->setSMTPAuth(true);

        if ($connection->getPort() === 465) {
            $mailer->setSMTPSecure('ssl');
        } elseif ($connection->getPort() === 587) {
            $mailer->setSMTPSecure('tls');
        } else {
            $mailer->setSMTPSecure('');
            $this->logger->notice('Port and host don\'t match.');
        }

        $mailer->setUserName($connection->getUserName());
        $mailer->setPassword($connection->getPassword());
        $mailer->setHost($connection->getHost());
        $mailer->setPort($connection->getPort());
        return $this;
    }

    /**
     * Sets the minimum time
     *
     * @param int $seconds
     * @return ContactForm
     */
    public function setTimeMinimum(int $seconds): self
    {
        $this->timeMinimum = $seconds;
        return $this;
    }

    /**
     * Sets the length of the submit session
     *
     * @param int $submitSessionLength
     * @return ContactForm
     */
    public function setSubmitSessionLength(int $submitSessionLength): self
    {
        $this->submitSessionLength = $submitSessionLength;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHandleRequestStrict(): bool
    {
        return $this->handleRequestStrict;
    }

    /**
     * @param bool $handleRequestStrict
     */
    public function setHandleRequestStrict(bool $handleRequestStrict): void
    {
        $this->handleRequestStrict = $handleRequestStrict;
    }

    /**
     * @return bool
     */
    private function proofRequest(): bool
    {
        return isset($_SERVER['REQUEST_METHOD']) && 'POST' === $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return Mail[]
     */
    public function getMails(): array
    {
        return $this->mails;
    }
}

<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Tests\Secure;

use ContactForm\Secure\UniqueToken;
use Exception;
use PHPUnit\Framework\TestCase;

class UniqueTokenTest extends TestCase
{
    /**
     * Tests if a token can be validated and expires after using once
     *
     * @throws Exception
     */
    public function testTokenCanBeValidatedAndExpires(): void
    {
        $uniqueToken = new UniqueToken();
        $token = $uniqueToken->getToken();
        
        self::assertTrue(
            $uniqueToken->isTokenValid($token)
        );

        self::assertFalse(
            $uniqueToken->isTokenValid($token)
        );
    }
}

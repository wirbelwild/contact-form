<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Configuration;

use ContactForm\Exception\NameNotAllowedException;

class Form
{
    private string $namespace;

    private string $nameFieldName;

    private string $emailFieldName;
   
    /**
     * @var array<string, string|array<mixed>>
     */
    private array $data = [
        'uniquetoken' => '',
        'timestamp' => '',
    ];
    
    private string $honeyPotFieldName;

    /**
     * @var array<int, string>
     */
    private array $sensitiveInputFields = [];

    /**
     * Defines the form.
     *
     * @param string $namespace The namespace of our form, from where we take all values.
     *                          For example `contact` when we have fields like `name="contact[firstname]"`.
     *                          Data outside that namespace will be ignored.
     * @param string $name      Name of the input field from where we get the name (without namespace!).
     * @param string $email     Name of the input field from where we get the email address (without namespace!).
     * @param string $honeyPot  Name of the input field which is used as honey pot (without namespace!).
     *                          This field will be invisible so a human won't fill it but a bot will.
     *                          The name is `fax` per default.
     * @throws NameNotAllowedException
     */
    public function __construct(
        string $namespace,
        string $name,
        string $email,
        string $honeyPot = 'fax'
    ) {
        $this->namespace = $namespace;
        
        if (array_key_exists($name, $this->data)
            || array_key_exists($email, $this->data)
            || array_key_exists($honeyPot, $this->data)
            || $name === $email
            || $name === $honeyPot
            || $email === $honeyPot
        ) {
            throw new NameNotAllowedException($name);
        }
        
        $this->nameFieldName = $name;
        $this->emailFieldName = $email;
        $this->honeyPotFieldName = $honeyPot;
    }
    
    /**
     * Returns the form namespace
     *
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }
    
    /**
     * Returns the input field name for the name information
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->getData($this->nameFieldName);
    }
    
    /**
     * Returns the input field name for the email information
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->getData($this->emailFieldName);
    }

    /**
     * @return string
     */
    public function getUniqueToken(): string
    {
        return $this->getData('uniquetoken');
    }

    /**
     * @return int
     */
    public function getTimeStamp(): int
    {
        return $this->getData('timestamp');
    }

    /**
     * @return string
     */
    public function getHoneyPotFieldName(): string
    {
        return $this->honeyPotFieldName;
    }
    
    /**
     * Maps the given content to their variables.
     *
     * @param string $key
     * @param string|array<mixed> $value
     * @return $this
     */
    public function setData(string $key, $value): self
    {
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * Returns all data or a specific one from the form.
     * Please note that the data is not validated and needs some security handling,
     * for example with `htmlspecialchars`.
     *
     * @param string|null $key The key to return a single part of the form.
     * @return mixed
     */
    public function getData(?string $key = null)
    {
        if (null === $key) {
            return $this->data;
        }
        
        $key = mb_strtolower($key);
        return $this->data[$key] ?? null;
    }

    /**
     * Defines one or multiple input fields as sensitive. This will exclude them from logging.
     *
     * @param string ...$inputField
     * @return $this
     */
    public function defineInputFieldSensitive(string ...$inputField): self
    {
        array_push($this->sensitiveInputFields, ...$inputField);
        return $this;
    }

    /**
     * @return array<int, string>
     */
    public function getSensitiveInputFields(): array
    {
        return $this->sensitiveInputFields;
    }
}

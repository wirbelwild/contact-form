# Changes in Bit&Black Contact Form v2.5

## 2.5.2 2021-01-20

### Changed

-   `Form::getData()` may return the whole dataset when not parameter is provided.

## 2.5.1 2021-01-20

### Fixed

-   Fixed error that could appear when form had nested array contents.

## 2.5.0 2021-01-18

### Added 

-   Added possibility to add attachments to single mails.

### Changed

-   Class `MailerInterface` has the methods `addAttachment` and `getAttachments` now. Also, some return types have been added.
<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Secure;

use Exception;

/**
 * Unique tokens for only one use.
 */
class UniqueToken
{
    /**
     * Create a unique token and adds it to session.
     *
     * @return string
     * @throws Exception
     */
    public function getToken(): string
    {
        $uniqueToken = random_bytes(16);
        $uniqueToken = bin2hex($uniqueToken);

        $_SESSION['contactForm']['uniqueToken'] = [];
        $_SESSION['contactForm']['uniqueToken'][$uniqueToken] = 1;
        
        return $uniqueToken;
    }
          
    /**
     * Returns if a unique token is valid
     * Deletes token from session the make it invalid
     *
     * @param string $uniqueToken
     * @return bool
     */
    public function isTokenValid(string $uniqueToken): bool
    {
        $uniqueTokens = $_SESSION['contactForm']['uniqueToken'] ?? [];
        
        if (array_key_exists($uniqueToken, $uniqueTokens)) {
            unset($uniqueTokens[$uniqueToken]);
            $_SESSION['contactForm']['uniqueToken'] = $uniqueTokens;
            return true;
        }
        
        return false;
    }
}

# Changes in Bit&Black Contact Form v3.0

## 3.0.0 2022-11-11

### Changed

-   PHP `>=7.4` is now required.
-   Added `psr/log@^3.0` to `composer.json` and changed return type of `ContactForm::setLogger` from `self` to `void`.
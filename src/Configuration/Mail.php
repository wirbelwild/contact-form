<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Configuration;

class Mail
{
    private string $subject;

    private string $receiver;
    
    private string $message;

    private ?string $replyToName = null;

    private ?string $replyToMail = null;
    
    private ?string $sender = null;

    private bool $html = false;

    /**
     * @var array<int, array{'path': string, 'name': string|null}>
     */
    private array $attachments = [];

    /**
     * Construct
     *
     * @param string $subject  The subject of the email.
     * @param string $receiver The receiver of the mail, needs to be a valid email address.
     * @param string $message  The message itself.
     */
    public function __construct(
        string $subject,
        string $receiver,
        string $message
    ) {
        $this->subject = $subject;
        $this->receiver = $receiver;
        $this->message = $message;
    }
    
    /**
     * Returns the mail subject
     *
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }
    
    /**
     * Returns the mail receiver
     *
     * @return string
     */
    public function getReceiver(): string
    {
        return $this->receiver;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
    
    /**
     * @return string|null
     */
    public function getReplyToName(): ?string
    {
        return $this->replyToName;
    }

    /**
     * @return string|null
     */
    public function getReplyToMail(): ?string
    {
        return $this->replyToMail;
    }

    /**
     * @param string $replyToMail Where the reply should go to, needs to be a valid email address.
     * @return $this
     */
    public function setReplyToMail(string $replyToMail): self
    {
        $this->replyToMail = $replyToMail;
        return $this;
    }

    /**
     * @param string $replyToName The name to where the replay should go.
     * @return $this
     */
    public function setReplyToName(string $replyToName): self
    {
        $this->replyToName = $replyToName;
        return $this;
    }

    /**
     * @param string $sender The sender.
     * @return $this
     */
    public function setSender(string $sender): self
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSender(): ?string
    {
        return $this->sender;
    }

    /**
     * @return bool
     */
    public function isHtml(): bool
    {
        return $this->html;
    }

    /**
     * @param bool $html
     * @return Mail
     */
    public function setHtml(bool $html): self
    {
        $this->html = $html;
        return $this;
    }

    /**
     * Adds an attachment to this email.
     *
     * @param string $path      The path to the file.
     * @param string|null $name The file name. This one is optional.
     * @return $this
     */
    public function addAttachment(string $path, ?string $name = null): self
    {
        $this->attachments[] = [
            'path' => $path,
            'name' => $name,
        ];
        return $this;
    }

    /**
     * @return array<int, array{'path': string, 'name': string|null}>
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }
}

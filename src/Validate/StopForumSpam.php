<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Validate;

use JsonException;

/**
 * Connects the StopForumSpam API
 */
class StopForumSpam implements ValidationInterface
{
    /**
     * The base url for the StopForumSpam API
     */
    private string $baseApiUrl = 'https://api.stopforumspam.org/api';

    /**
     * Whether to treat Tor Exit nodes as Spam
     */
    private bool $allowTorNodes;

    /**
     * The frequency of spam reports that a username/email/ip must
     * have to be considered spam, defaults to THRESHOLD_STRICT, which is 1 spam report
     */
    private int $frequencyThreshold;

    /**
     * Create a new StopForumSpam Object
     *
     * @param int $frequencyThreshold The frequency of spam reports that a username/email/ip must have
     *                                to be considered spam, defaults to THRESHOLD_STRICT, which is 1 spam report.
     * @param bool $allowTorNodes     Whether to treat Tor Exit nodes as spam.
     */
    public function __construct(int $frequencyThreshold = 1, bool $allowTorNodes = true)
    {
        $this->frequencyThreshold = $frequencyThreshold;
        $this->allowTorNodes = $allowTorNodes;
    }

    /**
     * Builds the URL for the spam isBad queries
     *
     * @param string $type  Ip|email the type of spam to check $value for.
     * @param string $value The ip, email or username to check for spam reports.
     * @return string       The full url to the api.
     */
    private function buildUrl(string $type, string $value): string
    {
        $type = strtolower(trim($type));

        $url = $this->baseApiUrl . '?' . $type . '=' . urlencode($value);

        if (!$this->allowTorNodes) {
            $url .= '&notorexit';
        }

        $url .= '&f=json';
        
        return $url;
    }

    /**
     * Sends a simple GET request to a URL and returns the response
     *
     * @param string $url The url to send a GET request to
     * @return string
     */
    private function sendRequest(string $url): string
    {
        if (function_exists('curl_version')
            && false !== $curl = curl_init($url)
        ) {
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            curl_close($curl);
            return (string) $response;
        }

        return (string) file_get_contents($url);
    }

    /**
     * Checks if an ip or email address is recognized bad
     *
     * @param string $type
     * @param string $value
     * @return bool
     */
    private function isBad(string $type, string $value): bool
    {
        $fullApiUrl = $this->buildUrl($type, $value);
        $response = $this->sendRequest($fullApiUrl);

        try {
            $json = json_decode($response, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $jsonException) {
            return true;
        }

        if (1 !== $json['success']) {
            return false;
        }

        if (1 === ($json[$type]['appears'] ?? null)) {
            return $json[$type]['frequency'] >= $this->frequencyThreshold;
        }
        
        return false;
    }

    /**
     * Function used to query the StopForumSpam API for an IP address and return if it is registered as a spamer IP.
     *
     * @param string $ipAddress The IP address to search the API for.
     * @return bool             True if IP is associated with spam, false if not.
     */
    public function isIpBad(string $ipAddress): bool
    {
        return $this->isBad('ip', $ipAddress);
    }

    /**
     * Function used to query the StopForumSpam API for an Email address and return if it is registered as a spam email.
     *
     * @param string $emailAddress The Email address to search the API for.
     * @return bool                True means the IP is a spammy email, if false, it's not.
     */
    public function isEmailBad(string $emailAddress): bool
    {
        return $this->isBad('email', $emailAddress);
    }
}

<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Tests\Validate;

use ContactForm\Validate\StopForumSpam;
use PHPUnit\Framework\TestCase;

class StopForumSpamTest extends TestCase
{
    /**
     * Tests if an ip can be validated
     */
    public function testIpCanBeValidated(): void
    {
        $stopForumSpam = new StopForumSpam();

        self::assertFalse(
            $stopForumSpam->isIpBad('87.183.251.132')
        );
    }
    
    /**
     * Tests if an email address can be validated
     */
    public function testEmailCanBeValidated(): void
    {
        $stopForumSpam = new StopForumSpam();

        self::assertFalse(
            $stopForumSpam->isEmailBad('hello@bitandblack.com')
        );
    }
}

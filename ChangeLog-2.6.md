# Changes in Bit&Black Contact Form v2.6

## 2.6.1 2022-11-11

### Fixed

-   Removed `psr/log@^3.0` from `composer.json` to prevent breaking change in `ContactForm::setLogger`.

## 2.6.0 2022-11-10

### Changed

-   Dependencies have been updated.
<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Tests;

use ContactForm\Configuration\Form;
use ContactForm\Configuration\Mail;
use ContactForm\Configuration\SMTP;
use ContactForm\ContactForm;
use ContactForm\Exception\NameNotAllowedException;
use DOMDocument;
use DOMElement;
use Exception;
use Iterator;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class ContactFormTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testCanSetSessionVariables(): void
    {
        $smtp = new SMTP('709fc8b0135c1d', '9dbd8257be31c9', 'smtp.mailtrap.io', 2525);
        $form = new Form('contact', 'name', 'email');
        
        $contactForm = new ContactForm($form, $smtp);
        
        self::assertArrayHasKey('vector', $_SESSION['contactForm']);
        self::assertArrayHasKey('key', $_SESSION['contactForm']);
        
        unset($contactForm);
    }

    /**
     * @throws Exception
     */
    public function testCanMapData(): void
    {
        $namespace = 'strangenamespace';
        $nameContent = 'Donald Duck';
        $emailContent = 'donald@duck.com';
        $messageContent = 'Quack';

        $form = new Form($namespace, 'strangenamefield', 'strangeemailfield');

        $contactForm = new ContactForm($form);
        $contactForm->setTimeMinimum(0);

        /**
         * Array for simulated post request
         */
        $post = [
            $namespace => [
                'strangenamefield' => $nameContent,
                'strangeemailfield' => $emailContent,
                'strangemessagefield' => $messageContent,
            ],
        ];

        $additionalFields = $contactForm->getAdditionalFields();

        $domDocument = new DOMDocument();
        $domDocument->loadHTML($additionalFields);

        $inputs = $domDocument->getElementsByTagName('input');

        /**
         * Adds the fields to the request array
         * @var DOMElement $input
         */
        foreach ($inputs as $input) {
            $name = $input->getAttribute('name');
            preg_match('/(?<=\[)([a-zA-Z0-9_]+)(?=\])/', $name, $name);
            $value = $input->getAttribute('value');
            $post[$namespace][$name[0]] = $value;
        }

        self::assertTrue(
            $contactForm->isSubmitValid($post),
            $contactForm->getStatus()
        );

        self::assertSame(
            $nameContent,
            $form->getName()
        );

        self::assertSame(
            $emailContent,
            $form->getEmail()
        );

        self::assertSame(
            $messageContent,
            $form->getData('strangemessagefield')
        );
    }
    
    /**
     * @throws Exception
     */
    public function testCanMapDataNested(): void
    {
        $namespace = 'strangenamespace';
        $nameContent = 'Donald Duck';
        $emailContent = 'donald@duck.com';
        $messageContent = [
            'first' => 'Quack11',
            'second' => [
                'Quack21',
                'Quack22',
            ],
        ];

        $form = new Form($namespace, 'strangenamefield', 'strangeemailfield');

        $contactForm = new ContactForm($form);
        $contactForm->setTimeMinimum(0);

        /**
         * Array for simulated post request
         */
        $post = [
            $namespace => [
                'strangenamefield' => $nameContent,
                'strangeemailfield' => $emailContent,
                'strangemessagefield' => $messageContent,
            ],
        ];

        $additionalFields = $contactForm->getAdditionalFields();

        $domDocument = new DOMDocument();
        $domDocument->loadHTML($additionalFields);

        $inputs = $domDocument->getElementsByTagName('input');

        /**
         * Adds the fields to the request array
         * @var DOMElement $input
         */
        foreach ($inputs as $input) {
            $name = $input->getAttribute('name');
            preg_match('/(?<=\[)([a-zA-Z0-9_]+)(?=\])/', $name, $name);
            $value = $input->getAttribute('value');
            $post[$namespace][$name[0]] = $value;
        }

        self::assertTrue(
            $contactForm->isSubmitValid($post),
            $contactForm->getStatus()
        );

        self::assertSame(
            $nameContent,
            $form->getName()
        );

        self::assertSame(
            $emailContent,
            $form->getEmail()
        );

        self::assertIsArray(
            $form->getData('strangemessagefield')
        );

        self::assertSame(
            $messageContent,
            $form->getData('strangemessagefield')
        );
    }

    /**
     * @param string $name
     * @throws NameNotAllowedException
     * @throws Exception
     * @dataProvider nameProvider()
     */
    public function testThrowsExceptionOnExistingName(string $name): void
    {
        $this->expectException(NameNotAllowedException::class);
        $form = new Form('contact', $name, $name);
        $contactForm = new ContactForm($form);
        unset($contactForm);
    }

    /**
     * @return array<int, array<int, string>>
     */
    public function nameProvider(): Iterator
    {
        yield ['spambot'];
        yield ['uniquetoken'];
        yield ['timestamp'];
        yield ['fax'];
    }

    /**
     * @throws NameNotAllowedException
     * @throws Exception
     */
    public function testHandlesHoneyPot(): void
    {
        $namespace = 'strangenamespace';
        $nameContent = 'Donald Duck';
        $emailContent = 'donald@duck.com';
        $messageContent = 'Quack';
        $honeyPotField = 'strangehoneypotfield';
        
        $form = new Form($namespace, 'strangenamefield', 'strangeemailfield', $honeyPotField);

        $contactForm = new ContactForm($form);
        $contactForm->setTimeMinimum(0);
        
        /**
         * Array for simulated post request
         */
        $post = [
            $namespace => [
                'strangenamefield' => $nameContent,
                'strangeemailfield' => $emailContent,
                'strangemessagefield' => $messageContent,
            ],
        ];

        $additionalFields = $contactForm->getAdditionalFields();

        $domDocument = new DOMDocument();
        $domDocument->loadHTML($additionalFields);

        $inputs = $domDocument->getElementsByTagName('input');

        /**
         * Adds the fields to the request array
         * @var DOMElement $input
         */
        foreach ($inputs as $input) {
            $name = $input->getAttribute('name');
            preg_match('/(?<=\[)([a-zA-Z0-9_]+)(?=\])/', $name, $name);
            $value = $input->getAttribute('value');
            $post[$namespace][$name[0]] = $value;
        }
        
        $post[$namespace][$honeyPotField] = 'Trapped';
        
        self::assertFalse(
            $contactForm->isSubmitValid($post),
            $contactForm->getStatus()
        );
    }

    /**
     * @throws NameNotAllowedException
     * @throws Exception
     */
    public function testAccessFormFields(): void
    {
        $namespace = 'strangenamespace';
        $nameContent = 'Donald Duck';
        $emailContent = 'donald@duck.com';
        $messageContent = 'Quack';
        $additionalContentKey = 'additionalContent';
        $additionalContentValue = 'Some additional content!';

        $form = new Form($namespace, 'strangenamefield', 'strangeemailfield');

        $contactForm = new ContactForm($form);
        $contactForm->setTimeMinimum(0);
        
        /**
         * Array for simulated post request
         */
        $post = [
            $namespace => [
                'strangenamefield' => $nameContent,
                'strangeemailfield' => $emailContent,
                'strangemessagefield' => $messageContent,
                $additionalContentKey => $additionalContentValue,
            ],
        ];
        
        $additionalFields = $contactForm->getAdditionalFields();

        $domDocument = new DOMDocument();
        $domDocument->loadHTML($additionalFields);

        $inputs = $domDocument->getElementsByTagName('input');

        /**
         * Adds the fields to the request array
         * @var DOMElement $input
         */
        foreach ($inputs as $input) {
            $name = $input->getAttribute('name');
            preg_match('/(?<=\[)([a-zA-Z0-9_]+)(?=\])/', $name, $name);
            $value = $input->getAttribute('value');
            $post[$namespace][$name[0]] = $value;
        }

        self::assertTrue(
            $contactForm->isSubmitValid($post),
            $contactForm->getStatus()
        );
        
        self::assertSame(
            $additionalContentValue,
            $form->getData($additionalContentKey)
        );
    }

    /**
     * @throws Exception
     */
    public function testCanLog(): void
    {
        $logger = new class() implements LoggerInterface {
            /**
             * @var array<int, array<string, mixed>>
             */
            private array $messages = [];

            /**
             * @param string $message
             * @param array<mixed> $context
             */
            private function append($message, array $context = []): void
            {
                $this->messages[] = [
                    'message' => $message,
                    'context' => $context,
                ];
            }
            
            /**
             * @inheritDoc
             */
            public function emergency($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function alert($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function critical($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function error($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function warning($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function notice($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function info($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function debug($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function log($level, $message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @return array<int, array<string, string>>
             */
            public function getMessages(): array
            {
                return $this->messages;
            }
        };
        
        $smtp = new SMTP('709fc8b0135c1d', '9dbd8257be31c9', 'smtp.mailtrap.io', 2525);
        $form = new Form('contact', 'name', 'email');

        $contactForm = new ContactForm($form, $smtp);
        $contactForm->setLogger($logger);
        
        $smtp->isSMTPConnectionValid();
        
        $messages = $logger->getMessages();
        
        self::assertNotEmpty($messages);

        $lastMessage = (array) end($messages);
        
        self::assertSame(
            'Finished SMTP check.',
            $lastMessage['message']
        );
    }

    /**
     * @throws Exception
     */
    public function testCanHandleMultipleMails(): void
    {
        $mail1 = new Mail('', 'receiver1', '');
        $mail2 = new Mail('', 'receiver2', '');

        $form = new Form('contact', 'name', 'email');
        $contactForm = new ContactForm($form);

        $contactForm->addMail(
            $mail1,
            $mail2
        );

        self::assertEquals(
            [
                $mail1,
                $mail2,
            ],
            $contactForm->getMails()
        );
    }

    /**
     * @throws Exception
     */
    public function testDoesntLogSensitiveInformation(): void
    {
        $logger = new class() implements LoggerInterface {
            /**
             * @var array<int, array<string, mixed>>
             */
            private array $messages = [];

            /**
             * @param string $message
             * @param array<mixed> $context
             */
            private function append($message, array $context = []): void
            {
                $this->messages[] = [
                    'message' => $message,
                    'context' => $context,
                ];
            }

            /**
             * @inheritDoc
             */
            public function emergency($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function alert($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function critical($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function error($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function warning($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function notice($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function info($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function debug($message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @inheritDoc
             */
            public function log($level, $message, array $context = []): void
            {
                $this->append($message, $context);
            }

            /**
             * @return array<int, array<string, string>>
             */
            public function getMessages(): array
            {
                return $this->messages;
            }
        };

        $form = new Form('contact', 'name', 'email');
        $form->defineInputFieldSensitive('password');

        $namespace = $form->getNamespace();

        $contactForm = new ContactForm($form);
        $contactForm->setLogger($logger);

        $submit = [
            $namespace => [
                'name' => 'Donald Duck',
                'email' => 'quack@example.org',
                'password' => 'Duckburg5000!',
            ],
        ];

        $contactForm->isSubmitValid($submit);

        $messages = $logger->getMessages();

        self::assertSame(
            'Donald Duck',
            $messages[1]['context'][1]
        );

        self::assertSame(
            'quack@example.org',
            $messages[2]['context'][1]
        );

        self::assertSame(
            '*** SENSITIVE INFORMATION ***',
            $messages[3]['context'][1]
        );
    }
}

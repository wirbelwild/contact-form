<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WelcomeCommand extends Command
{
    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('welcome')
            ->setDescription('Displays the welcome message.')
            ->setHelp('This command displays the welcome message.')
        ;
    }

    /**
     * Displays a welcome message
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Welcome to the Contact Form Console!');
        return 0;
    }
}

<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Tests\Secure;

use ContactForm\Secure\Crypt;
use Exception;
use PHPUnit\Framework\TestCase;

class CryptTest extends TestCase
{
    /**
     * Tests if a string can be encrypted and decrypted correctly
     *
     * @throws Exception
     */
    public function testEncryptAndDecryptString(): void
    {
        $crypt = new Crypt();
        $crypt->createSalt();
        
        $testStringOriginal = 'This is a test.';
        
        $testStringHashed = $crypt->encryptData($testStringOriginal);
        $testStringReturned = $crypt->decryptData($testStringHashed);
        
        self::assertSame(
            $testStringOriginal,
            $testStringReturned
        );
    }
}

<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Mailer;

/**
 * The MailerInterface provides the handling and configuration of email sending libraries.
 */
interface MailerInterface
{
    /**
     * Returns the mailer itself. This helps to access all methods and variables which maybe not covered
     * by the mailers bridge class. But be careful, so define things directly may case different values
     * between the bridge class and the mailer class. So if the bridge has the needed setter or getter it is
     * always better to prefer that method.
     *
     * @return object
     */
    public function getMailer(): object;
    
    /**
     * Sets the senders email address.
     *
     * @param string $fromAddress
     * @return self
     */
    public function setFromAddress(string $fromAddress): self;

    /**
     * Gets the senders email address.
     *
     * @return string|null
     */
    public function getFromAddress(): ?string;

    /**
     * Sets the senders name.
     *
     * @param string $fromName
     * @return self
     */
    public function setFromName(string $fromName): self;

    /**
     * Gets the senders name.
     *
     * @return string|null
     */
    public function getFromName(): ?string;

    /**
     * Sets the email subject.
     *
     * @param string $subject
     * @return self
     */
    public function setSubject(string $subject): self;

    /**
     * Gets the email subject.
     *
     * @return string|null
     */
    public function getSubject(): ?string;

    /**
     * Sets the receivers email address.
     *
     * @param string $toAddress
     * @return self
     */
    public function setToAddress(string $toAddress): self;

    /**
     * Gets the receivers email address.
     *
     * @return string|null
     */
    public function getToAddress(): ?string;

    /**
     * Sets the email address for replies.
     *
     * @param string $replyToAddress
     * @return self
     */
    public function setReplyToAddress(string $replyToAddress): self;

    /**
     * Gets the email address for replies.
     *
     * @return string|null
     */
    public function getReplyToAddress(): ?string;

    /**
     * Sets the name for replies.
     *
     * @param string $replyToName
     * @return self
     */
    public function setReplyToName(string $replyToName): self;

    /**
     * Gets the name for replies.
     *
     * @return string|null
     */
    public function getReplyToName(): ?string;

    /**
     * Sets if the SMTP connection should be debugged.
     *
     * @param bool $debugSMTP
     * @return self
     */
    public function setDebugSMTP(bool $debugSMTP): self;

    /**
     * Gets if the SMTP connection should be debugged.
     *
     * @return bool
     */
    public function isDebugSMTP(): bool;

    /**
     * Sets if the mail should be send as html.
     *
     * @param bool $html
     * @return self
     */
    public function setHTML(bool $html): self;

    /**
     * Gets if the mail should be send as html.
     *
     * @return bool
     */
    public function isHTML(): bool;

    /**
     * Sets the plain email message.
     *
     * @param string $message
     * @return self
     */
    public function setMessage(string $message): self;

    /**
     * Gets the plain email message.
     *
     * @return string|null
     */
    public function getMessage(): ?string;

    /**
     * Sets the html email message.
     *
     * @param string $messageHTML
     * @return self
     */
    public function setMessageHTML(string $messageHTML): self;

    /**
     * Gets the html email message.
     *
     * @return string|null
     */
    public function getMessageHTML(): ?string;

    /**
     * Sends the email.
     *
     * @return bool
     */
    public function send(): bool;

    /**
     * Enables or disables the use of SMTP.
     *
     * @param bool $smtp
     * @return self
     */
    public function setSMTP(bool $smtp): self;

    /**
     * Gets if SMTP is enabled or not.
     *
     * @return bool
     */
    public function isSMTP(): bool;

    /**
     * Enables or disabled thr SMTP authentication.
     *
     * @param bool $smtpAuth
     * @return self
     */
    public function setSMTPAuth(bool $smtpAuth): self;

    /**
     * Gets if SMTP authentication is enabled or not.
     *
     * @return bool
     */
    public function isSMTPAuth(): bool;

    /**
     * Sets the SMTP protocol.
     *
     * @param string $smtpSecure
     * @return self
     */
    public function setSMTPSecure(string $smtpSecure): self;

    /**
     * Gets the SMTP protocol.
     *
     * @return string|null
     */
    public function getSMTPSecure(): ?string;

    /**
     * Sets the SMTP user name.
     *
     * @param string $userName
     * @return self
     */
    public function setUserName(string $userName): self;

    /**
     * Gets the SMTP user name.
     *
     * @return string|null
     */
    public function getUserName(): ?string;

    /**
     * Sets the SMTP password.
     *
     * @param string $password
     * @return self
     */
    public function setPassword(string $password): self;

    /**
     * Gets the SMTP password.
     *
     * @return string|null
     */
    public function getPassword(): ?string;

    /**
     * Sets the SMTP host.
     *
     * @param string $host
     * @return self
     */
    public function setHost(string $host): self;

    /**
     * Gets the SMTP host.
     *
     * @return string|null
     */
    public function getHost(): ?string;

    /**
     * Sets the port.
     *
     * @param int $port
     * @return self
     */
    public function setPort(int $port): self;

    /**
     * Gets the port.
     *
     * @return int|null
     */
    public function getPort(): ?int;

    /**
     * Sets the charset.
     *
     * @param string $charSet
     * @return self
     */
    public function setCharSet(string $charSet): self;

    /**
     * Gets the charset.
     *
     * @return string|null
     */
    public function getCharSet(): ?string;

    /**
     * Sets the content type definition.
     *
     * @param string $contentType
     * @return self
     */
    public function setContentType(string $contentType): self;

    /**
     * Gets the content type definition.
     *
     * @return string|null
     */
    public function getContentType(): ?string;

    /**
     * Sets the email encoding.
     *
     * @param string $encoding
     * @return self
     */
    public function setEncoding(string $encoding): self;

    /**
     * Gets the email encoding.
     *
     * @return string|null
     */
    public function getEncoding(): ?string;

    /**
     * Sets the word wrap number.
     *
     * @param int $wordWrap
     * @return self
     */
    public function setWordWrap(int $wordWrap): self;

    /**
     * Gets the word wrap number.
     *
     * @return int|null
     */
    public function getWordWrap(): ?int;

    /**
     * Returns an array with debug information
     *
     * @return array<mixed>
     */
    public function getDebugInformation(): array;

    /**
     * Sets the senders email address. This is mostly also the return path.
     *
     * @param string $sender
     * @return self
     */
    public function setSender(string $sender): self;

    /**
     * Gets the senders email address.
     *
     * @return string|null
     */
    public function getSender(): ?string;

    /**
     * Adds an attachment.
     *
     * @param string $path
     * @param string|null $name
     * @return self
     */
    public function addAttachment(string $path, ?string $name = null): self;

    /**
     * Returns an array of attachments.
     *
     * @return array<int, mixed>
     */
    public function getAttachments(): array;
}

<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Secure;

use Exception;

/**
 * Encrypting and decrypting strings
 */
class Crypt
{
    /**
     * The secret key
     */
    private string $key = '';

    /**
     * The secret iv
     */
    private string $vector = '';

    /**
     * Encryption method
     */
    private string $encryptMethod = 'AES-256-CBC';

    /**
     * Creates random key and iv strings
     *
     * @return bool
     * @throws Exception
     */
    public function createSalt(): bool
    {
        /** @var int<1, max>|false $vectorLength */
        $vectorLength = openssl_cipher_iv_length($this->encryptMethod);
        
        if (false === $vectorLength) {
            $vectorLength = 16;
        }

        $this->vector = random_bytes($vectorLength);
        $this->key = (string) time();
        
        return '' !== $this->vector;
    }

    /**
     * Sets the iv
     *
     * @param string $vector
     * @return Crypt
     */
    public function setVector(string $vector): self
    {
        $this->vector = $vector;
        return $this;
    }

    /**
     * Sets the key
     *
     * @param string $key
     * @return Crypt
     */
    public function setKey(string $key): self
    {
        $this->key = $key;
        return $this;
    }
    
    /**
     * Gets the vector
     *
     * @return string
     */
    public function getVector(): string
    {
        return $this->vector;
    }
    
    /**
     * Gets the key
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }
    
    /**
     * Encrypts data
     *
     * @param string $inputRaw
     * @return string
     */
    public function encryptData(string $inputRaw): string
    {
        $hash = openssl_encrypt(
            $inputRaw,
            $this->encryptMethod,
            $this->key,
            0,
            $this->vector
        );

        return base64_encode((string) $hash);
    }
    
    /**
     * Decrypts data
     *
     * @param string $inputHash
     * @return string
     */
    public function decryptData(string $inputHash): string
    {
        $inputHash = base64_decode($inputHash);
        
        $input = openssl_decrypt(
            (string) $inputHash,
            $this->encryptMethod,
            $this->key,
            0,
            $this->vector
        );
        
        return (string) $input;
    }
}

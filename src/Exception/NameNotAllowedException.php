<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Exception;

use ContactForm\Exception;
use Throwable;

class NameNotAllowedException extends Exception
{
    /**
     * NameNotAllowedException constructor.
     *
     * @param string $name
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $name, int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct('Name "' . $name . '" is not allowed as it already exists.', $code, $previous);
    }
}

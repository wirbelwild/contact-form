<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Validate;

/**
 * Interface ValidationInterface.
 */
interface ValidationInterface
{
    /**
     * @param string $emailAddress
     * @return bool
     */
    public function isEmailBad(string $emailAddress): bool;

    /**
     * @param string $ipAddress
     * @return bool
     */
    public function isIpBad(string $ipAddress): bool;
}

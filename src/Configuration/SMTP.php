<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Configuration;

use PHPMailer\PHPMailer\SMTP as MailerSMTP;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class SMTP implements LoggerAwareInterface
{
    private string $userName;

    private string $password;

    private string $host;

    private int $port;
    
    private LoggerInterface $logger;

    /**
     * Construct
     *
     * @param string $userName The username, mostly an email address.
     * @param string $password The password.
     * @param string $host     The host, may contain `smtp.`.
     *                         Also be sure to declare the scheme like `ssl://` or `tls://` at the beginning.
     * @param int    $port     The port number.
     */
    public function __construct(
        string $userName,
        string $password,
        string $host,
        int $port
    ) {
        $this->userName = $userName;
        $this->password = $password;
        $this->host = $host;
        $this->port = $port;
        $this->logger = new NullLogger();
    }
    
    /**
     * Returns the SMTP username
     *
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }
    
    /**
     * Returns the SMTP password
     *
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
    
    /**
     * Returns the SMTP host
     *
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }
    
    /**
     * Returns the SMTP port
     *
     * @return int
     */
    public function getPort(): int
    {
        return $this->port;
    }
    
    /**
     * Checks SMTP connection
     *
     * @return bool
     */
    public function isSMTPConnectionValid(): bool
    {
        if (!extension_loaded('openssl')) {
            $this->logger->debug('Missing extension "openssl".');
            return false;
        }

        $smtp = new MailerSMTP();
        $smtp->do_debug = MailerSMTP::DEBUG_LOWLEVEL;
        $smtp->setDebugOutput($this->logger);
        
        if (!$smtp->connect($this->getHost(), $this->getPort(), 10)) {
            $smtp->quit(true);
            $this->logger->debug('Cannot connect host.', [$this->getHost(), $this->getPort()]);
            return false;
        }

        if (!$smtp->hello($this->getHost())) {
            $smtp->quit(true);
            $this->logger->debug('Helo failed.');
            return false;
        }

        $esmtpServices = $smtp->getServerExtList();

        if (is_array($esmtpServices) && array_key_exists('STARTTLS', $esmtpServices)) {
            $smtp->startTLS();
            $smtp->hello($this->getHost());
            $esmtpServices = $smtp->getServerExtList();
        }

        if (is_array($esmtpServices) && array_key_exists('AUTH', $esmtpServices)
            && !$smtp->authenticate($this->getUserName(), $this->getPassword())
        ) {
            $smtp->quit(true);
            $this->logger->debug('AUTH failed.');
            return false;
        }

        $smtp->quit(true);
        $this->logger->debug('Finished SMTP check.', [
            'esmtpServices' => $esmtpServices,
        ]);
        return null !== $esmtpServices;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}

<?php

/**
 * ContactForm – Easy preventing spambots
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace ContactForm\Mailer;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class PHPMailerBridge implements MailerInterface
{
    private PHPMailer $mailer;

    private ?string $replyToAddress = null;

    private ?string $replyToName = null;

    private ?string $toAddress = null;

    private ?string $toName = null;

    private ?string $fromName = null;

    private ?string $fromAddress = null;

    private ?string $subject = null;

    private bool $debugSMTP = false;

    private bool $html = false;

    private ?string $message = null;

    private ?string $messageHTML = null;

    private bool $smtp = false;

    private ?bool $smtpAuth = null;

    private ?string $smtpSecure = null;

    private ?string $userName = null;

    private ?string $password = null;

    private ?string $host = null;

    private ?int $port = null;

    private ?string $charSet = null;

    private ?string $contentType = null;

    private ?string $encoding = null;

    private ?int $wordWrap = null;

    private ?string $sender = null;

    /**
     * PHPMailerBridge constructor.
     */
    public function __construct()
    {
        $this->mailer = new PHPMailer();
    }

    /**
     * Clones the mailer also to get a new one without the previous settings.
     */
    public function __clone()
    {
        $this->mailer = clone $this->mailer;
    }

    /**
     * Returns the mailer itself. This helps to access all methods and variables which maybe not covered
     * by the mailers bridge class. But be careful, so define things directly may case different values
     * between the bridge class and the mailer class. So if the bridge has the needed setter or getter it is
     * always better to prefer that method.
     *
     * @return PHPMailer
     */
    public function getMailer(): PHPMailer
    {
        return $this->mailer;
    }

    /**
     * @param string $fromAddress
     * @return PHPMailerBridge
     */
    public function setFromAddress(string $fromAddress): MailerInterface
    {
        $this->fromAddress = $fromAddress;
        $this->mailer->From = $fromAddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFromAddress(): ?string
    {
        return $this->fromAddress;
    }

    /**
     * @param string $fromName
     * @return PHPMailerBridge
     */
    public function setFromName(string $fromName): MailerInterface
    {
        $this->fromName = $fromName;
        $this->mailer->FromName = $fromName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFromName(): ?string
    {
        return $this->fromName;
    }

    /**
     * @param string $subject
     * @return PHPMailerBridge
     */
    public function setSubject(string $subject): MailerInterface
    {
        $this->subject = $subject;
        $this->mailer->Subject = $subject;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string $toAddress
     * @return PHPMailerBridge
     * @throws Exception
     */
    public function setToAddress(string $toAddress): MailerInterface
    {
        $this->toAddress = $toAddress;
        $this->mailer->addAddress($this->toAddress, $this->toName);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getToAddress(): ?string
    {
        return $this->toAddress;
    }

    /**
     * @param string $toName
     * @return PHPMailerBridge
     * @throws Exception
     */
    public function setToName(string $toName): MailerInterface
    {
        $this->toName = $toName;
        $this->mailer->addAddress($this->toAddress, $this->toName);
        return $this;
    }

    /**
     * @return string
     */
    public function getToName(): string
    {
        return $this->toName;
    }

    /**
     * @param string $replyToAddress
     * @return PHPMailerBridge
     * @throws Exception
     */
    public function setReplyToAddress(string $replyToAddress): MailerInterface
    {
        $this->replyToAddress = $replyToAddress;
        $this->mailer->addReplyTo($this->replyToAddress, $this->replyToName);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReplyToAddress(): ?string
    {
        return $this->replyToAddress;
    }

    /**
     * @param string $replyToName
     * @return PHPMailerBridge
     * @throws Exception
     */
    public function setReplyToName(string $replyToName): MailerInterface
    {
        $this->replyToName = $replyToName;
        $this->mailer->addReplyTo($this->replyToAddress, $this->replyToName);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReplyToName(): ?string
    {
        return $this->replyToName;
    }

    /**
     * @param bool $debugSMTP
     * @return PHPMailerBridge
     */
    public function setDebugSMTP(bool $debugSMTP): MailerInterface
    {
        $this->debugSMTP = $debugSMTP;
        $this->mailer->SMTPDebug = false === $debugSMTP ? 0 : 2;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDebugSMTP(): bool
    {
        return $this->debugSMTP;
    }

    /**
     * @param bool $html
     * @return PHPMailerBridge
     */
    public function setHTML(bool $html): MailerInterface
    {
        $this->html = $html;
        $this->mailer->IsHTML($html);
        return $this;
    }

    /**
     * @return bool
     */
    public function isHTML(): bool
    {
        return $this->html;
    }

    /**
     * @param string $message
     * @return PHPMailerBridge
     */
    public function setMessage(string $message): MailerInterface
    {
        $this->message = $message;
        $this->mailer->AltBody = $message;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $messageHTML
     * @return PHPMailerBridge
     * @throws Exception
     */
    public function setMessageHTML(string $messageHTML): MailerInterface
    {
        $this->messageHTML = $messageHTML;
        $this->mailer->msgHTML($messageHTML, __DIR__, true);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessageHTML(): ?string
    {
        return $this->messageHTML;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function send(): bool
    {
        return $this->mailer->send();
    }

    /**
     * @param bool $smtp
     * @return PHPMailerBridge
     */
    public function setSMTP(bool $smtp): MailerInterface
    {
        $this->smtp = $smtp;

        if ($smtp) {
            $this->mailer->IsSMTP();
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isSMTP(): bool
    {
        return $this->smtp;
    }

    /**
     * @param bool $smtpAuth
     * @return PHPMailerBridge
     */
    public function setSMTPAuth(bool $smtpAuth): MailerInterface
    {
        $this->smtpAuth = $smtpAuth;
        $this->mailer->SMTPAuth = $smtpAuth;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSMTPAuth(): bool
    {
        return $this->smtpAuth;
    }

    /**
     * @param string $smtpSecure
     * @return PHPMailerBridge
     */
    public function setSMTPSecure(string $smtpSecure): MailerInterface
    {
        $this->smtpSecure = $smtpSecure;
        $this->mailer->SMTPSecure = $smtpSecure;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSMTPSecure(): ?string
    {
        return $this->smtpSecure;
    }

    /**
     * @param string $userName
     * @return PHPMailerBridge
     */
    public function setUserName(string $userName): MailerInterface
    {
        $this->userName = $userName;
        $this->mailer->Username = $userName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserName(): ?string
    {
        return $this->userName;
    }

    /**
     * @param string $password
     * @return PHPMailerBridge
     */
    public function setPassword(string $password): MailerInterface
    {
        $this->password = $password;
        $this->mailer->Password = $password;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $host
     * @return PHPMailerBridge
     */
    public function setHost(string $host): MailerInterface
    {
        $this->host = $host;
        $this->mailer->Host = $host;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHost(): ?string
    {
        return $this->host;
    }

    /**
     * @param int $port
     * @return PHPMailerBridge
     */
    public function setPort(int $port): MailerInterface
    {
        $this->port = $port;
        $this->mailer->Port = $port;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPort(): ?int
    {
        return $this->port;
    }

    /**
     * @param string $charSet
     * @return PHPMailerBridge
     */
    public function setCharSet(string $charSet): MailerInterface
    {
        $this->charSet = $charSet;
        $this->mailer->CharSet = $charSet;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCharSet(): ?string
    {
        return $this->charSet;
    }

    /**
     * @param string $contentType
     * @return PHPMailerBridge
     */
    public function setContentType(string $contentType): MailerInterface
    {
        $this->contentType = $contentType;
        $this->mailer->ContentType = $contentType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContentType(): ?string
    {
        return $this->contentType;
    }

    /**
     * @param string $encoding
     * @return PHPMailerBridge
     */
    public function setEncoding(string $encoding): MailerInterface
    {
        $this->encoding = $encoding;
        $this->mailer->Encoding = $encoding;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEncoding(): ?string
    {
        return $this->encoding;
    }

    /**
     * @param int $wordWrap
     * @return PHPMailerBridge
     */
    public function setWordWrap(int $wordWrap): MailerInterface
    {
        $this->wordWrap = $wordWrap;
        $this->mailer->WordWrap = $wordWrap;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWordWrap(): ?int
    {
        return $this->wordWrap;
    }

    /**
     * Returns an array with debug information
     *
     * @return array<int, string>
     */
    public function getDebugInformation(): array
    {
        return [$this->mailer->ErrorInfo];
    }

    /**
     * @inheritDoc
     */
    public function setSender(string $sender): MailerInterface
    {
        $this->sender = $sender;
        $this->mailer->Sender = $sender;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSender(): ?string
    {
        return $this->sender;
    }

    /**
     * @param string $path
     * @param string|null $name
     * @return $this
     * @throws Exception
     */
    public function addAttachment(string $path, ?string $name = null): MailerInterface
    {
        $this->mailer->addAttachment($path, $name ?? '');
        return $this;
    }

    /**
     * @return array<int, mixed>
     */
    public function getAttachments(): array
    {
        return $this->mailer->getAttachments();
    }
}

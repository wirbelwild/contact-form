[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/contact-form)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/ba15f9d35ae44e628237fe7a4162b618)](https://www.codacy.com/bb/wirbelwild/contact-form/dashboard)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/contact-form/v/stable)](https://packagist.org/packages/bitandblack/contact-form)
[![Total Downloads](https://poser.pugx.org/bitandblack/contact-form/downloads)](https://packagist.org/packages/bitandblack/contact-form)
[![License](https://poser.pugx.org/bitandblack/contact-form/license)](https://packagist.org/packages/bitandblack/contact-form)

# Contact Form 

Making your contact form much safer.

## What is it for?

The Contact Form script makes your contact form safer. It 

*   Checks if a form has been submitted in less than 5 seconds and blocks the submit
*   Allows only one submit for one session
*   Adds a honeypot for spambots
*   Checks if IP or email address are known as bad

## Installation

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/contact-form). Add it to your project by running `$ composer require bitandblack/contact-form`. 

## Usage

Set up your SMTP connection:

````php
<?php 

use ContactForm\Configuration\SMTP;
 
$smtp = new SMTP('709fc8b0135c1d', '9dbd8257be31c9', 'smtp.mailtrap.io', 2525);
````

Set up the form and tell the namespace and the names of the fields for the name and the email address:

````php
<?php
 
use ContactForm\Configuration\Form;
 
$form = new Form('contact', 'name', 'email');
````

Init Contact Form then: 

````php
<?php

use ContactForm\ContactForm;

$contactForm = new ContactForm($form, $smtp);
````

Add the logic to validate the submit, init the mail and send the form:

````php
<?php 

use ContactForm\Configuration\Mail;

$mailSentInCurrentSubmit = false;
$mailSentInPreviousSubmit = false;

if ($contactForm->isSubmitValid($_POST)) {
    $mail = new Mail(
        'New message from your website', 
        'you@example.org', 
        'There is a message for you: ' . $form->getData('message')
    );

    $mail 
        ->setReplyToName($form->getName())
        ->setReplyToMail($form->getEmail())
    ;

    $contactForm->addMail($mail);
    $mailSentInCurrentSubmit = $contactForm->sendMail();
}

$mailSentInPreviousSubmit = $contactForm->hasSentMailPreviously();
````

It's possible to add multiple mails here.

Add the additional fields by calling `$contactForm->getAdditionalFields()` to you form. For example:

````html
<form action="" method="post">
    <input type="text" name="contact[name]" title="Your Name" placeholder="John Doe">
    <input type="email" name="contact[email]" title="Your Email Address" placeholder="john.doe@example.org">
    <textarea name="contact[message]" title="Your Message" placeholder="Dear Sir or Madame, ..."></textarea>
    <button type="submit">Send</button>
    <?php echo $contactForm->getAdditionalFields(); ?>
</form>
````

Add a statement to show a message somewhere on top of your form: 

````php
<?php 
 
if ($mailSentInCurrentSubmit || $mailSentInPreviousSubmit) {
    echo '
        <p>
            Your mail has been sent.
        </p>
    '; 
}
````

If you want to see the script all in one take a look in the `example` folder. 

### Mailer 

Contact Form uses PHPMailer per default. You can set up your own mailer by using `$contactForm->setMailer(new MyCustomMailer())`. The mailer needs to implement the `MailerInterface`. When sending mails Contact Form will clone this object to always have the same configuration.

### External Spam Validation 

Contact Form provides the validation of IP and Email address by the StopFormSpam API. To enable this you need to call `$contactForm->addSpamValidation(new \ContactForm\Validate\StopForumSpam())`. You can set up your own validation in the same way. Every validation object needs to implement the `ValidationInterface`.

## Help 

If you have questions targeting the usage of Contact Form, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).